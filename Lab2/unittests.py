import unittest
from card import Card

class ExampleTest(unittest.TestCase):

  def setUp(self):
    self.aceClubs = Card(1,1)
    # self.aceClubs = Card(1,1)

  def test_getSuit(self):
    self.assertEqual(1,self.aceClubs.getSuit())
 
  def test_getRank(self):
    self.assertEqual(1,self.aceClubs.getRank())

  def test_getValue(self):
    self.assertEqual(0,self.aceClubs.getValue())  



class ExampleTest2(unittest.TestCase):

  def setUp(self):
    self.aceClubs = Card(1,1)
    # self.aceClubs = Card(1,1)

  def test_getSuit(self):
    print('Example2 pass')
    if(self.assertEqual(1,self.aceClubs.getSuit())):
      print('Example2 pass')
 
  def test_getRank(self):
    self.assertEqual(1,self.aceClubs.getRank())

  def test_getValue(self):
    self.assertEqual(0,self.aceClubs.getValue())  




class ExampleTest3(unittest.TestCase):

  def setUp(self):
    self.aceClubs = Card(1,1)
    # self.aceClubs = Card(1,1)

  def test_getSuit(self):
    print('Example3 pass')
    if(self.assertEqual(1,self.aceClubs.getSuit())):
      print('Example3 pass')
 
  def test_getRank(self):
    self.assertEqual(1,self.aceClubs.getRank())

  def test_getValue(self):
    self.assertEqual(0,self.aceClubs.getValue())      
  
################################################# 
# Can add all Classes and Tests This way.. 
#################################################  
# suite = unittest.TestSuite()
# suite.addTest(unittest.makeSuite(ExampleTest))
# suite.addTest(unittest.makeSuite(ExampleTest2))
# suite.addTest(unittest.makeSuite(ExampleTest3))

# runner = unittest.TextTestRunner()
# res=runner.run(suite)

# ################################################# 
# Can add new Test Classes to the Array
# ################################################# 
def suite():
      ## can add more suites to array. 
      suites = [ExampleTest, ExampleTest2, ExampleTest3]
      suite = unittest.TestSuite()
      for s in suites:
          suite.addTest(unittest.makeSuite(s))
      return suite 

runner = unittest.TextTestRunner()
res=runner.run(suite())

################################################## 

print(res)
print("*"*20)
for i in res.failures: print(i[1])