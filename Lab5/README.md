## WARNING!: DO NOT PUSH DIRECTLY TO MASTER! .......  CHECKOUT A BRANCH AFTER CLONING WITH THE COMMAND
    - git checkout -b random_name_you_choose_for_your_branch
push back to the repo all you want, **when you are done with your branch, it can be merged back into master.**

Additionally, you may copy .gitignore file into new directories you make so that you don't push python library requirements, or any .idea files.
- http://gitignore.io/ with django,pycharm+all generates a git ignore file with syntax to ignore user specific directories or log files that should not be pushed 
- message from C.K. had to remove with " git rm --cached -r .idea"
- if git is not ignoring something try running "git rm --cached -r filename"
- check what the git ignore file is ignoring with "git status --ignored"

## Basic Git Commands
when you start editing code... 

1. clone the repo with the argument, this will make a new folder in a directory 
    - **git clone https://USERNAME@bitbucket.org/USERNAME/red_ryders.git**

2. checkout a branch and make your edits in the new branch with 
    - **git checkout -b random_name_you_choose_for_your_branch**

3. after you make changes and want to add them to the repo there are three steps
    - **git add .**
    - **git commit -m "add your message"**
    - **git push origin random_name_you_choose_for_your_branch**
    
4. you can also just use the pycharm IDE buttons

**note, again, please don't push directly to master**

