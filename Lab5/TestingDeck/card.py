class Card:
  def __init__(self, s, r):
    #initialize a card to the given
     #suit (1-4) #rank (1-13)
    self.s = s
    self.r = r
    self.value = 0
    #1:clubs 2:spades 3:hearts 4:diamonds
    #throw an exception for invalid argument

  def getSuit(self):
    #return the suit of the card (1-4)
    return self.s
  def getRank(self):
    #return the rank of the card (1-13)
    return self.r
  def getValue(self):
    #get the game-specific value of a Card
    #if not used, or game is not defined, always returns 0.
    return self.value
  def __str__(self):
    #return rank and suite, as AS for ace of spades, or 3H for
    #three of hearts, JC for jack of clubs.
    return ""