import unittest
from unittest import mock
import card, deck

def mockDeckGenerator():
    ############################################
    # Creates a valid mock deck, validMockDeck
    ############################################
    validMockDeck = []
    for currentSuit in range(1, 5):  # range is not inclusive
        for currentRank in range(1, 14):
            mockCard = mock.Mock()
            mockCard.getSuit = mock.Mock(return_value=currentSuit)
            mockCard.getRank = mock.Mock(return_value=currentRank)
            validMockDeck.append(mockCard)

    return validMockDeck
    ############################################

# Card tests
class CardInitCorrectRankAndSuit(unittest.TestCase):
    def test_GetRankAndGetSuitAccuracy(self):
        # create card
        fakeCard = card.Card(3, 4)  # create a card whose suit is 3 and rank is 4
        # test(s)
        self.assertEqual(fakeCard.getSuit(), 3)  # check that, if the card's suit is 3, its getSuit() method returns 3
        self.assertEqual(fakeCard.getRank(), 4)  # check that, if the card's rank is 4, its getRank() method returns 4
        self.assertNotEqual(fakeCard.getSuit(), 4)  # check that getSuit() does not return rank
        self.assertNotEqual(fakeCard.getRank(), 3)  # check that getRank() does not return suit

class CardInitRaiseValueError(unittest.TestCase):
    def test_RaiseValueError(self):
        self.testCard = card.Card(1,1)
        self.assertEqual(0, self.testCard.getValue()) # more needed once Poker Card Values Set

class CardGetSuite(unittest.TestCase):
    # This test will be to test that the getSuit() method of a card will return an accurate value
    def test_GetSuitAccuracy(self):
        # create card
        fakeCard = card.Card(1, 1)  # creating a card

        # test(s)
        self.assertEqual(fakeCard.getSuit(), 1)  # check that, if the card's suit is 1, its getSuit() method returns 1

    # This test is to ensure that no suit is outside of the acceptable range
    def test_GetSuitRange(self):
        # Note: for now the card's suit is being pre-selected, but in a version with a working card.py there will
        # instead be a for loop that draws a large amount of cards, ensuring that all are within the acceptable range.
        # create card
        fakeCard = card.Card(1, 1)  # creating a card

        # test(s)
        self.assertLessEqual(fakeCard.getSuit(), 4)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(fakeCard.getSuit(), 1)  # acceptable range of suits (1-4).


class CardGetRank(unittest.TestCase):
    # This test will be to test that the getRank() method of a card will return an accurate value
    def test_GetRankAccuracy(self):
        # mock card
        fakeCard = card.Card(1, 9)  # creating a card

        # test(s)
        self.assertEqual(fakeCard.getRank(), 9)

    def test_GetRankRange(self):
        # Note: for now the card's rank is being pre-selected, but in a version with a working card.py there will
        # instead be a for loop that draws a large amount of cards, ensuring that all are within the acceptable range.
        # mock card
        fakeCard = card.Card(1, 9)  # creating a card

        # test(s)
        self.assertLessEqual(fakeCard.getRank(), 13)  # This will be used to make sure any card drawn is within the
        self.assertGreaterEqual(fakeCard.getRank(), 1)  # acceptable range of ranks (1-13).


class CardGetValue(unittest.TestCase):
    # Initialize some Cards, and assign them a value and check that it worked
    def test_CardGetValue(self):
        aceSpades = card.Card(2, 1)
        aceClubs = card.Card(1, 1)
        # I now have two random cards
        # My card objects are Named aceSpades, aceClubs

        # if we need values in a certain range, maybe assign a ridiculous value and then
        # check it didn't work. Right now nothing is there

        # testing that card value is initializing to default value (0)
        self.assertEqual(0, aceClubs.getValue())
        self.assertEqual(0, aceSpades.getValue())


class CardStr(unittest.TestCase):

    def test_Str(self):
        self.aceClubs = card.Card(1,1)
        # intentional failures, until __str__ is implemented correctly in card.py
        # Because __str__ is unimplemented, it will always be blank, rather than the correct String.
        self.assertEquals(self.aceClubs.__str__(), "AC")
        self.twoClubs = card.Card(1,2)
        self.assertEquals(self.twoClubs.__str__(), "2C")

# deck tests
class DeckInit(unittest.TestCase):

    def test_DeckInit(self):
        validMockDeck = mockDeckGenerator()
        testDeck = deck.Deck()

        ###########################################################################
        # Testing a valid deck, validMockDeck
        ###########################################################################
        # test that every card in the valid mockDeck passes by making sure every card in the deck has a suit between 1 and 4, and a rank between 1 and 3, and that no card is repeated.
        validMockDeckTestIsPassing = True
        realDeckTestIsPassing = True
        cardsInDeckForValidMockDeckTest = []
        numCards = 0

        # goes through every card in the deck
        for currentCard in validMockDeck:
            numCards = numCards + 1  # to count number of cards in the deck

            # puts the rank and suit of the card in a list format
            currentCardFormatted = [currentCard.getRank(), currentCard.getSuit()]
            # should remove cards in order, equality should be set when deck is instantiated
            realCard = testDeck.draw()

            self.assertEqual(currentCard.getRank(), realCard.getRank())
            self.assertEqual(currentCard.getSuit(), realCard.getSuit())

            # If the current card being processed is already in the deck, it's a duplicate, so it's an invalid deck, so the test fails.
            if currentCardFormatted in cardsInDeckForValidMockDeckTest:
                validMockDeckTestIsPassing = False

            # adds the current card after it was formatted to the list to check for duplicates
            cardsInDeckForValidMockDeckTest.append(currentCardFormatted)

            # if suit or rank are not in range, test fails
            if ((currentCard.getSuit() < 1) or (currentCard.getSuit() > 4)):
                validMockDeckTestIsPassing = False
            elif ((currentCard.getRank() < 1) or (currentCard.getRank() > 13)):
                validMockDeckTestIsPassing = False

            # if suit or rank are not in range, test fails
            if ((realCard.getSuit() < 1) or (realCard.getSuit() > 4)):
                realDeckTestIsPassing = False
            elif ((realCard.getRank() < 1) or (realCard.getRank() > 13)):
                realDeckTestIsPassing = False

                # if there are not 52 cards in the deck, it is invalid, so the test fails.
        if numCards != 52:
            validMockDeckTestIsPassing = False

        if numCards != 52:
            realDeckTestIsPassing = False

        # If it passed all the tests above, it should pass the assertTrue
        self.assertTrue(validMockDeckTestIsPassing)
        self.assertTrue(realDeckTestIsPassing)
        # print(validMockDeckTestIsPassing)
        ###########################################################################


class DeckCount(unittest.TestCase):
    def test_init_empty_deck(self):

        expected_response = 0  # expected response of empty deck
        actual_response = mock.Mock()  # create fake deck until deck.py is finished

        for x in range(52):  # empty out deck
            actual_response.draw()

        self.assertEqual(expected_response, actual_response.count)  # check for empty deck

        # test count for proper full deck of 52.

    def test_init_full_deck(self):
        expected_response = 52
        actual_response = mock.Mock()  # create fake deck until deck.py is finished
        self.assertEqual(expected_response, actual_response.count())  # check for full deck

        # test for count less than 0

    def test_init_non_positive(self):
        actual_response = deck.Deck()  # create fake deck
        for x in range(60):  # empty out deck and draw more than 52 cards
            actual_response.draw()

        self.assertGreaterEqual(actual_response.count(), 0)  # count should be >= 0

        # test for count greater than 52

    def test_init_overstock_deck(self):
        actual_response = deck.Deck()  # create fake deck
        self.assertLessEqual(actual_response.count(), 52)

        # Test count of draws from 52 to 0

    def test_init_semi_deck(self):
        expected_response = 52
        actual_response = mock.Mock()  # fake deck

        # initial testing of 52 cards
        self.assertEqual(expected_response, actual_response.count())

        for x in range(52):
            actual_response.draw()  # draw a card
            expected_response = expected_response - 1  # decrement deck count by 1.
            self.assertEqual(expected_response, actual_response.count())  # check for correct count


class DeckDrawValue(unittest.TestCase):

    def test_init_draw(self):
        # A new initialized deck should have cards in order
        expected_response = card.Card(1, 1)  # check implementation for card order#
        actual_response = deck.Deck()  # Mock a fake deck
        # Deck stored in python array with append()
        # Pop() top card from deck is assumed to be used
        # Expected card drawn is King of Diamonds.
        self.assertEquals(expected_response, actual_response.draw())

    def test_init_ValueError(self):
        actual_response = deck.Deck()  # fake deck, empty out deck
        for x in range(53):
            actual_response.draw()  # draw a card
        try:
            actual_response.draw()  # attempt to draw card from empty deck
        except ValueError:
            # Expected value error to be thrown
            print("Value Error is raised")
            pass
        except AssertionError:
            # Proper value error is not thrown. Raise assertion error.
            print("Value Error is not raised")

    def test_inRange(self):
        testDeck = deck.Deck()
        self.assertGreaterEqual(1, testDeck.draw().getValue())  # Assume the values can only return from 1-13 based on a standard card game
        self.assertLessEqual(13, testDeck.draw().getValue())

class DeckDrawException(unittest.TestCase):

    def test_Draw(self):
        # Drawing cards from a sorted deck, the draw should work in order top to bottom
        validMockDeck = mockDeckGenerator()
        TestDeck = deck.Deck()
        deckPosition = 0

        for x in range(1,53):
            # 1 - 52  non - inclusive
            card = TestDeck.draw()
            self.assertEqual(card, validMockDeck[deckPosition])
            deckPosition = deckPosition + 1

            if card in TestDeck:
                # After Card is drawn, it should be removed from the deck
                raise ValueError

class DeckShuffle(unittest.TestCase):
    # test shuffle method of deck class
    # if shuffle method works, the decks should not be equal
    def test_init_shuffle(self):
        actual_response1 = deck.Deck()  # fake deck 1
        actual_response2 = actual_response1.shuffle()  # shuffle second deck
        self.assertNotEqual(actual_response1, actual_response2)  # check that decks are not equal


def suite():
    # Add all the classes, each class will be a different "test Suite" just a group of tests to see if everything in the class is functioning correctly
    suites = [CardInitCorrectRankAndSuit, CardInitRaiseValueError, CardGetSuite, CardGetRank, CardGetValue, CardStr,
              DeckInit, DeckCount, DeckDrawValue, DeckDrawException, DeckShuffle]
    suite = unittest.TestSuite()
    for s in suites:
        suite.addTest(unittest.makeSuite(s))
        # this code will just loop through an array with all the class names and add them to the suite object.
    return suite


suite = suite()