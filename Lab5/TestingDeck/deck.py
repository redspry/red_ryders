import card

class Deck:
  def __init__(self):
    #create a standard 52 card deck of cards

    # The logic should be in here to create a deck of cards
    # The deck when first created will be in order of smallest to largest
    pass

  def count(self):
    #return the number of cards remaining in the deck
    return 0

  def draw(self):
    #return and remove the top card in the deck
    #if the deck is empty, raise a ValueError
    return card.Card(1,1)

  def shuffle(self):
    #shuffle the deck using a random number generator
    pass